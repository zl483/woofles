Woofles

Woofles is an online dog sitter networking service created for CS338 offered at Drexel in the Summer of 2022. This project was developed by Jackie Holowka and Zoe Levine, with a fair bit of referencing to the courses provided projects.

What it is

Woofles is a web app designed to connect dog owners and dog sitters.

How to Run

We were unable to get it running using uWSGI - (traded email with the Professor and TA, however it was unclear why this wasn't working). So, to run, make sure you have python3, flask, and passlib (as we have in the virtual environment in /home/zl483/public_html/uwsgi/env/). From there, run `python3 woofles/run.py` which will begin hosting the site on the port **10070**. Currently, the active line in run.py for starting the application is `app.run(host='0.0.0.0', port=10070, debug=True)` which can be used to run it on tux. Once hosting begins, the terminal will tell you exactly what the address is (this will be something like http://10.246.251.13:10070). For using loopback, you can comment out the previously mentioned `app.run` line and uncomment `app.run(host='127.0.0.1', port=10070, debug=True)`.

Again, unfortunately we were unable to get uWSGI working, but we believe this is a sufficient alternative for the scope of this class.
