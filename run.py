from flask import Flask, render_template, send_file, g, request, jsonify, session, escape, redirect
from passlib.hash import pbkdf2_sha256
import os
from db import Database

# Provided by the Professor, not written by us (just changed as necessary to fit this apps needs)

app = Flask(__name__, static_folder='public', static_url_path='')
app.secret_key = b'lkj98t&%$3rhfSwu3D'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = Database()
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/')
def index():
    if 'user' in session.keys():
        return render_template('login.html', logged_in=True)
    else:
        return render_template('login.html', logged_in=False)

# @app.route('/course/<path:path>')
# def base_static(path):
#     return send_file(os.path.join(app.root_path, '..', 'course', path))


# @app.route('/api/goats')
# def api_goats():
#     if 'user' in session:
#         n = request.args.get('n', default=6)
#         offset = request.args.get('offset', default=0)
#         response = {
#             'goats': get_db().get_goats(n, offset),
#             'total': get_db().get_total_goat_count()
#         }
#         return jsonify(response)
#     else:
#         return jsonify('Error: User not authenticated')


# @app.route('/api/adopt')
# def api_adopt():
#     if 'user' in session:
#         goat_id = request.args.get('uid')
#         user_id = session['user']['user_id']
#         get_db().update_goat_adopted(goat_id, user_id)
#         return api_goats()
#     else:
#         return jsonify('Error: User not authenticated')

# @app.route('/api/mygoats')
# def api_mygoats():
#     if 'user' in session:
#         user_id = session['user']['user_id']
#         response = get_db().get_user_goats(user_id)
#         return jsonify(response)
#     else:
#         return jsonify('Error: User not authenticated')

# @app.route('/api/unadopt')
# def api_unadopt():
#     if 'user' in session:
#         goat_id = request.args.get('uid')
#         get_db().update_goat_adopted(goat_id, 0)
#         return api_mygoats()
#     else:
#         return jsonify('Error: User not authenticated')

@app.route('/create_dog_profiles', methods=['GET', 'POST'])
def create_dog_profiles():
    if request.method == 'POST':
        if request.form["action"] == "cancel":
            get_db().remove_user(session['user']['username'])
            session.pop('user', None)
            return redirect('/login')
        elif request.form["action"] == "continue":
            dogs = {}
            for i in request.form:
                if "-" in i:
                    key_split = i.split("-")
                    if key_split[1] in dogs:
                        dogs[key_split[1]][key_split[0]] = request.form[i]
                    else:
                        dogs[key_split[1]] = {key_split[0] : request.form[i]}
            for dog in dogs.values():
                name = dog['name']
                age = dog['age']
                breed = dog['breed']
                food_sentence = "Eats {} {}\n".format(dog['food_qt'], dog['food_occ'])
                activity_sentence = "Activity Level: {}/5\n".format(dog['activity'])
                notes = food_sentence + activity_sentence + dog['notes']
                dog_id = get_db().create_dog(name, age, breed, notes)
                get_db().add_dog(session['user']['username'], dog_id)
            return redirect('/my_profile')
    return render_template('create_dog_profiles.html')

@app.route('/create_account', methods=['GET', 'POST'])
def create_account():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        typed_password = request.form['password']
        addr = request.form['addr']
        bio = request.form['bio']
        if email and typed_password:
            encrypted_password = pbkdf2_sha256.encrypt(typed_password, rounds=200000, salt_size=16)
            get_db().create_user(name, email, encrypted_password, addr, bio)
            session['user'] = get_db().get_user(email)
            return redirect('/create_dog_profiles')
    return render_template('create_account.html')

@app.route('/my_profile', methods = ['GET', 'POST'])
def my_profile():
    session['user']['dog_ids'] = get_db().get_user(session['user']['username'])['dog_ids']
    user = session['user']
    dogs = []
    if (user['dog_ids'] == None) or (user['dog_ids'] == "") or (user['dog_ids'] == " "):
        return render_template('my_profile.html', user=user, dogs=None, logged_in=True, profile=True)
    else:
        for dog in user['dog_ids'].split(" "):
            dogs.append(get_db().get_dog(dog))
        return render_template('my_profile.html', user=user, dogs=dogs, logged_in=True, profile=True)


@app.route('/edit_profile', methods = ['GET', 'POST'])
def edit_profile():
    # make sure we have the most recent account information
    session['user'] = get_db().get_user(session['user']['username'])
    if request.method == 'POST':
        if request.form["action"] == "cancel":
            return redirect('/my_profile')
        if request.form["action"] == "save":
            user_id = session['user']['user_id']
            data = request.form
            # updating user information
            if data['name'] != session['user']['name']:
                get_db().update_user(user_id, 'name', data['name'])
            if data['email'] != session['user']['username']:
                get_db().update_user(user_id, 'username', data['email'])
            if data['password'] != '':
                encrypted_password = pbkdf2_sha256.encrypt(data['password'], rounds=200000, salt_size=16)
                get_db().update_user(user_id, 'encrypted_password', encrypted_password)
            if data['addr'] != session['user']['address']:
                get_db().update_user(user_id, 'address', data['addr'])
            if data['bio'] != session['user']['bio']:
                get_db().update_user(user_id, 'bio', data['bio'])
            prev_dog_ids = session['user']['dog_ids']
            new_dogs = {}
            existing_dogs = {}
            for i in request.form:
                if "-" in i:
                    key_split = i.split("-")
                    print(key_split)
                    if key_split[0] == "new":
                        if key_split[2] in new_dogs:
                            new_dogs[key_split[2]][key_split[1]] = request.form[i]
                        else:
                            new_dogs[key_split[2]] = {key_split[1] : request.form[i]}
                    else:
                        if key_split[1] in prev_dog_ids:
                            prev_dog_ids = prev_dog_ids.replace(key_split[1], '')
                        if key_split[1] in existing_dogs:
                            existing_dogs[key_split[1]][key_split[0]] = request.form[i]
                        else:
                            existing_dogs[key_split[1]] = {key_split[0] : request.form[i]}
            for dog in new_dogs.values():
                name = dog['name']
                age = dog['age']
                breed = dog['breed']
                food_sentence = "Eats {} {}\n".format(dog['food_qt'], dog['food_occ'])
                activity_sentence = "Activity Level: {}/5\n".format(dog['activity'])
                notes = food_sentence + activity_sentence + dog['notes']
                dog_id = get_db().create_dog(name, age, breed, notes)
                get_db().add_dog(session['user']['username'], dog_id)
            for dog_id, dog in existing_dogs.items():
                print(dog_id)
                print(dog)
                original_dog = get_db().get_dog(dog_id)[0]
                print(original_dog)
                # dog
                # {'name': 'b', 'age': '2', 'breed': 'b', 'notes': 'Eats b b\r\nActivity Level: 2/5\r\nb'}
                # original dog
                # {'name': 'b', 'age': 2, 'breed': 'b', 'bio': 'Eats b b\nActivity Level: 2/5\nb', 'sittings-booked': None}
                if dog['name'] != original_dog['name']:
                    get_db().update_dog(dog_id, 'name', dog['name'])
                if dog['age'] != original_dog['age']:
                    get_db().update_dog(dog_id, 'age', dog['age'])
                if dog['breed'] != original_dog['breed']:
                    get_db().update_dog(dog_id, 'breed', dog['breed'])
                if data['bio'] != original_dog['bio']:
                    get_db().update_dog(dog_id, 'bio', dog['notes'])
            print(prev_dog_ids)
            print(prev_dog_ids.split(" "))
            dogs_to_remove = [i for i in prev_dog_ids.split(" ") if i]
            for dog_id in dogs_to_remove:
                if dog_id != None:
                    print("removing dog")
                    print(dog_id)
                    get_db().remove_dog(session['user']['username'], dog_id)
            return redirect('/my_profile')
    else:
        session['user']['dog_ids'] = get_db().get_user(session['user']['username'])['dog_ids']
        user = session['user']
        dogs = []
        if (user['dog_ids'] == None) or (user['dog_ids'] == "") or (user['dog_ids'] == " "):
            return render_template('edit_profile.html', user=user, dogs=None, logged_in=True)
        else:
            for dog in user['dog_ids'].split(" "):
                dogs.append(get_db().get_dog(dog))
            return render_template('edit_profile.html', user=user, dogs=dogs, logged_in=True)

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        email = request.form['email']
        typed_password = request.form['password']
        if email and typed_password:
            user = get_db().get_user(email)
            if user:
                if pbkdf2_sha256.verify(typed_password, user['encrypted_password']):
                    session['user'] = user
                    return redirect('/my_profile')
                else:
                    error = "Incorrect password, please try again"
            else:
                error = "Unknown user, please try again"
        elif email and not typed_password:
            error = "Missing password, please try again"
        elif not email and typed_password:
            error = "Missing username, please try again"
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')

@app.route('/search', methods=['GET', 'POST'])
def search():
    if request.method == "POST":
        search = request.form['search']
        data = get_db().get_sitters(search)
        return render_template('search.html', data=data, logged_in=True, profile=False)
    return render_template('search.html', logged_in=True, profile=False)


@app.route('/profile', methods=['GET', 'POST'])
def profile():
    if request.method == "POST":
        #need to pass username through button then this should work
        username = request.form['username']
        user = get_db().get_user(username)
        return render_template('profile.html', user=user, logged_in=True, profile=False)

# @app.route('/<name>')
# def generic(name):
#     if 'user' in session:
#         return render_template(name + '.html')
#     else:
#         return redirect('/login')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=10070, debug=True)
    #app.run(host='10.246.250.148', port=10070, debug=True)
    #app.run(host='127.0.0.1', port=10070, debug=True)
