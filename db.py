import os
import re
import sqlite3
from unicodedata import name

# Provided by the Professor, not written by us (just changed as necessary to fit this apps needs)

SQLITE_PATH = os.path.join(os.path.dirname(__file__), 'woofles.db')


class Database:

    def __init__(self):
        self.conn = sqlite3.connect(SQLITE_PATH)

    def select(self, sql, parameters=[]):
        c = self.conn.cursor()
        c.execute(sql, parameters)
        return c.fetchall()

    def execute(self, sql, parameters=[]):
        c = self.conn.cursor()
        print(parameters)
        print(sql)
        c.execute(sql, parameters)
        self.conn.commit()

    def execute_return_id(self, sql, parameters=[]):
        c = self.conn.cursor()
        id = c.execute(sql, parameters).lastrowid
        self.conn.commit()
        return id

    # def get_goats(self, n, offset):
    #     data = self.select(
    #         'SELECT * FROM goats ORDER BY uid ASC LIMIT ? OFFSET ?', [n, offset])
    #     return [{
    #         'uid': d[0],
    #         'name': d[1],
    #         'age': d[2],
    #         'adopted': d[3],
    #         'image': d[4]
    #     } for d in data]

    # def get_user_goats(self, user_id):
    #     data = self.select(
    #         'SELECT * FROM goats WHERE adopted=? ORDER BY uid ASC', [user_id])
    #     return [{
    #         'uid': d[0],
    #         'name': d[1],
    #         'age': d[2],
    #         'adopted': d[3],
    #         'image': d[4]
    #     } for d in data]

    # def get_total_goat_count(self):
    #     data = self.select('SELECT COUNT(*) FROM goats')
    #     return data[0][0]

    # def update_goat_adopted(self, goat_id, user_id):
    #     self.execute('UPDATE goats SET adopted=? WHERE uid=?', [user_id, goat_id])

    def create_user(self, name, username, encrypted_password, address, bio):
        self.execute('INSERT INTO users (name, username, encrypted_password, address, bio, dog_ids) VALUES (?, ?, ?, ?, ?, ?)',
                     [name, username, encrypted_password, address, bio, 'None'])

    def remove_user(self, username):
        self.execute('DELETE FROM users WHERE username=?', [username])

    def update_user(self, user_id, column, new_value):
        self.execute('UPDATE users SET {col}=? WHERE user_id=?'.format(col = column),
                     [new_value, user_id])

    def create_dog(self, name, age, breed, notes):
        id = self.execute_return_id('INSERT INTO dogs (name, age, breed, bio) VALUES (?, ?, ?, ?)',
                        [name, age, breed, notes])
        return id
        # if data:
        #     d = data[0]
        #     print(d)
        #     return {d}
        # else:
        #     return None

    def update_dog(self, dog_id, column, new_value):
        self.execute('UPDATE dogs SET {col}=? WHERE dog_id=?'.format(col = column),
                     [new_value, dog_id])

    def add_dog(self, username, new_dog_id):
        user = self.get_user(username)
        if user['dog_ids'] == "" or user['dog_ids'] == 'None':
            all_dogs = new_dog_id
        else:
            all_dogs = user['dog_ids'] + " " + str(new_dog_id)
        self.execute('UPDATE users SET dog_ids=? WHERE user_id=?',
                     [all_dogs, user['user_id']])

    def get_user(self, username):
        data = self.select('SELECT * FROM users WHERE username=?', [username])
        if data:
            d = data[0]
            print(d)
            return {
                'user_id': d[0],
                'name': d[1],
                'username': d[2],
                'encrypted_password': d[3],
                'address': d[4],
                'bio': d[5],
                'cal_id': d[6],
                'dog_ids': d[7],
                'sittings_booked': d[8]
            }
        else:
            return None


    def get_sitters(self, search=False):
        if not search:
            data = self.select('SELECT * FROM users WHERE dog_ids=?', ['None'])
        else:
            data = self.select('SELECT * FROM users WHERE dog_ids=? AND address=? OR name=?', ['None', search, search])

        return data


    def get_dog(self, dog_id):
        data = self.select('SELECT * FROM dogs WHERE dog_id=?', [dog_id])
        if data:
            d= data[0]
            return [{
                'dog_id': d[0],
                'name': d[1],
                'age': d[2],
                'breed': d[3],
                'bio': d[4],
                'sittings-booked': d[5]
            }]
        else:
            return None

    def remove_dog(self, username, dog_id):
        self.execute('DELETE FROM dogs WHERE dog_id=?', [dog_id])
        user = self.get_user(username)
        all_dogs = user['dog_ids'].split(" ")
        all_dogs.remove(dog_id)
        self.execute('UPDATE users SET dog_ids=? WHERE user_id=?',
                     [" ".join(all_dogs), user['user_id']])

    def close(self):
        self.conn.close()
